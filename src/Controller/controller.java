package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.SimulationManager;

import View.QueueView;



public class controller implements ActionListener {
	
	private QueueView view;
	private SimulationManager model;
	
	public controller(QueueView view,SimulationManager model) {
		this.view=view;
		this.model=model;
		
		view.start.addActionListener(this);
		view.stop.addActionListener(this);
		
	}
	
	private int timpMin, timpMax, timpSim, timpLibMin, timpLibMax;
	private int nrCoziMaxime = 3;
	private int nrClientiMaxim;
	private joanna.queue.LogOfEvents loe;

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == view.start) {
		//	timpMin = Integer.parseInt(tTimpMin.getText());
			timpSim = Integer.parseInt(QueueView.jt1.getText());
			timpMax = Integer.parseInt(QueueView.jt2.getText());
			timpMin = Integer.parseInt(QueueView.jt3.getText());
			//timpMax = Integer.parseInt(QueueView.jt2.getText());
			//timpLibMin = Integer.parseInt(QueueView.jt3.getText());
			timpLibMax = Integer.parseInt(QueueView.jt4.getText());
			timpLibMin = Integer.parseInt(QueueView.jt5.getText());
			//timpSim = Integer.parseInt(QueueView.jt5.getText());
			nrClientiMaxim = Integer.parseInt(QueueView.jt6.getText());
			loe = new joanna.queue.LogOfEvents();
			
			SimulationManager test = new SimulationManager(nrCoziMaxime, timpSim, timpMin, timpMax, timpLibMin, timpLibMax, nrClientiMaxim,loe);
			test.start();
			//System.out.println("Test:" +nrCoziMaxime +"m" + timpSim +"m" + timpMin +"m"+ timpMax +"m"+ timpLibMin +"m"+ timpLibMax +"m"+ nrClientiMaxim);
			
		}
		
		if(e.getSource() == view.stop) {
			System.exit(1);
		}
		
	}

}
