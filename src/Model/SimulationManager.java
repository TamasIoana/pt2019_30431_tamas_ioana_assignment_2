package Model;


import java.util.Random;
import java.util.Date;
import java.util.Timer;

import joanna.queue.LogOfEvents;



public class SimulationManager extends Thread{
	
	private int nrCashReg;//numarul de case 
	static int SimulationInt;//simulation interval
	private int minArrT;//min arrival time between customers
	private int maxArrT;//max arrival time between customers 
	private int minServiceT;
	private int maxServiceT;
	private LogOfEvents loe;
	
	private CashRegister[] cashReg;
	static int ClientsNr=0;//id.ul clientului
	
	private Timer timer=new Timer();
	private int pos;
	
	int RemoveC=1;//remove clients
	int AddC=0;
	
	//constructor
	public SimulationManager(int nrCashReg,int SimulationInt,int minServiceT,int maxServiceT,int minArrT,int maxArrT,int AddC, LogOfEvents loe) {
		
		cashReg=new CashRegister[nrCashReg];
		this.nrCashReg=nrCashReg;
		SimulationManager.SimulationInt=SimulationInt;
		this.minArrT=minArrT;
		this.maxArrT=maxArrT;
		this.minServiceT=minServiceT;
		this.maxServiceT=maxServiceT;
		this.AddC=AddC;
		this.loe=loe;//for the log of events
		
	int i=0;
	while(i<nrCashReg) {
		cashReg[i]=new CashRegister();
		i++;
	  }
	}
	
	/*
	 * getter methods 
	 */
	
	public int getNrCashReg() {
		return nrCashReg;		
		
	}
	
	public int getSimulationInt() {
		return SimulationInt;
	}
	
	public CashRegister getCashReg(int i) {
		return cashReg[i];
	}
	
	
	
	/*
	 * metoda care returneaza numarul casei care are lungimea cea mai scurta 
	 */
	private int minQueue() {
		//long min=cashReg[0].queueLength();
		pos=0;
		long min=cashReg[0].queueLength();
      for(int i=0;i<nrCashReg;i++) {
		if(cashReg[i].queueLength()<min) {
			min=cashReg[i].queueLength();
			pos=i;
			}
   }
		return pos;
	}
	
	
	
	public void run() {
		
		// Synchronizing blocks instead of entire methods. Also
		// demonstrates protection of a non-thread-safe class
		// with a thread-safe one.
		synchronized(timer) {
			int pauza;
			
			//atata timp cat intervalul de simulare e mai mare ca zero 
			//sau atata tmp cat exista clienti in coada 
			while((SimulationInt>0 )|| (RemoveC<AddC)) {
				Random rand1=new Random();
				
				pauza=rand1.nextInt(maxArrT-minArrT)+minArrT;
				
				if(SimulationInt>0 || (RemoveC<AddC)) {
					
					//parcurge cozile 
					for(int i=0;i<nrCashReg;i++) {
						
						Random rand=new Random();
						CashRegister casa;//se creeaza o casa 
						ClientsNr++;//creste id.ul inainte pentru ca se pune de la clientul 1 
						
						//se creeaza un client nou 
						//se genereaza random service time pentru clientul cu id.ul ClientsNr 
						Client c=new Client(ClientsNr,new Date(),rand.nextInt(maxServiceT-minServiceT)+minServiceT);
						
						casa=cashReg[i];//numarul casei
						//System.out.println("Am adaugat la casa "+ i +" clientul cu nr "+ClientsNr+ " la data "+c.getArrivalT()+" cu timpul de servire "+c.getServiceT()) ;
						loe.appendText("Am adaugat la casa "+ i +" clientul cu nr "+ClientsNr+ " la data "+c.getArrivalT()+" cu timpul de servire "+c.getServiceT()+"\n");
						casa.addClient(c);//adauga client la casa 
					try{
						sleep(c.getServiceT() *1000);//face sleep pentru 1 secunda 
						remove();//si apoi elimina clientul de la coada 
					}
					catch(InterruptedException e) {
						System.out.println(SimulationManager.class.getName());
					}
					
					
					
					pauza=rand.nextInt(maxArrT-minArrT)+minArrT;
                    try { 
						
						sleep(pauza * 100);
					}
					
					catch(InterruptedException e) {
						System.out.println(SimulationManager.class.getName());
					}
                    
                    SimulationManager.SimulationInt -= pauza;
                    }
					
					//dupa ce clientul a terminat de platit(aka service time) acesta va fi eliminat din coada 
				}remove();
				
					
					}
					
				}
			}
	
	//metoda pentru eliminarea clientului din coada 
	public void remove() {
		//when we use synchronized, one thread will wait for the termination of the other
		synchronized(timer){
		Thread t=new Thread(){
			public void run() {
				int max=0;
				int pos=0;
				//se cauta casa cu lungimea cea mai mare 
				for(int i=0;i<nrCashReg;i++) {
					if(cashReg[i].queueLength()>max) {
						max= (int) cashReg[i].queueLength();//max devine casa cu lungimea cea mai mare 
						pos=i;
					}
					
				}
				
				CashRegister casa=cashReg[pos];
				//verifica daca coada nu este goala, deci daca are de unde elimina clienti 
				if(casa.Empty()==false) {
					//alege clientul de la pozitia 0 adica primul intrat si il scoate din coada 
					Client c=(casa.clients.get(0));
					
				SimulationManager.SimulationInt-=c.getServiceT();//simulation interval va scadea pentru ca un client a parasit coada 
				casa.removeClient();//elimina clientul 
				RemoveC++;//creste numarul de clienti eliminati
				//System.out.println("Client: "+c.getID()+" left.Time: "+new Date());//afiseaza mesajul cu id.ul clientului eliminat si data 
				loe.appendText("Client: "+c.getID()+" left.Time: "+new Date()+"\n");
				}
			}
		
		};
		//start a thread by calling its run method
		t.start();}	
	}
			
		

}
