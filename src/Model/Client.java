gpackage Model;

import java.util.Date;

public class Client {
	
	/*This class contains information for the client
	 * @clientID-a unique number to identify each client
	 * @serviceT-the time that the client will need to spend for its service
	 * 
	 */
	
	private int clientID;
	private int serviceT;
	private Date arrivalT;
	

	//constructor of the class
	public Client(int clientID, Date arrivalT,int serviceT) {
		this.clientID=clientID;
		this.serviceT=serviceT;
		this.arrivalT=arrivalT;
	}
	/*
	 * getID() returns the id of the client
	 * 	 */
	public int getID() {
		return clientID;
	}
	/*
	 * getServiceT() returns the service processing time
	 */
	
	public int getServiceT() {
		return serviceT;
	}
	
	/*
	 * getArrivalT returns the arrival time of the client
	 */
	public Date getArrivalT() {
		return arrivalT;
	}
	
	/*
	 * toString() method to get the format for each client
	 */
	@Override
	public String toString() {
		return "Client "+ clientID+"service time:"+serviceT;
	}
	
	

}
