package Model;
import java.util.*;

public class CashRegister {
	
	 ArrayList<Client> clients;
	
	 //constructor for the class
	 public CashRegister() {
		clients=new ArrayList<Client>();
	 }
	 
	 
		/*
		 * method that adds a client to the queue 
		 */
	 public  void addClient(Client client)  {
		 clients.add(client);
		 //notifyAll();
	 }
	 
	 /*
	  * method that removes a client and it first checks if the queue is empty 
	  * if not it can remove the client
	  */
	 public  void removeClient()  {
		 if (clients.isEmpty()==false)
			 clients.remove(0);
	 }
	 
	 /*
	  * method that returns the size of the queue
	  */
	 public long queueLength()  {
		 long size=clients.size();
		 return size;
	 }


	public boolean Empty() {
		boolean ok;
		if(clients.isEmpty()==true)
			ok=true;
			else ok=false;
		
		return ok;
	}
	 

}
