package joanna.queue;

import javax.swing.UIManager;

import Controller.controller;
import Model.SimulationManager;

import View.QueueView;

public class Main {
	
public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch (Exception e) {
			e.printStackTrace();
		} 
		/*		
		InterfataGrafica intGraf = new InterfataGrafica();
		intGraf.setSize(1224,560);
		intGraf.setVisible(true);
	*/
		
		 
		 QueueView view= new QueueView();
		 SimulationManager model=null;
		 new controller(view, model);
			view.setVisible(true);
			
		//private LogOfEvents loe;
		 //(int nrCashReg,int SimulationInt,int minServiceT,int maxServiceT,int minArrT,int maxArrT,int AddC)
		//SimulationManager queue = new SimulationManager(5, 50, 2, 8, 3, 5, 10);
		//queue.start();
		
    }

}
