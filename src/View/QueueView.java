package View;

import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class QueueView  extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	 Box mainBox, hBox1, hBox2, hBox3, hBox4,hBox5,hBox6,hBox7,hBox8;
	 JLabel  jl1=new JLabel("Simulation Time: ");
	 public static JTextField jt1  = new JTextField(10);
	 JLabel jl2=new JLabel(" Max Arrival Time: ");
	 public static JTextField jt2 = new JTextField(10);
	 JLabel jl3=new JLabel(" Min Arrival Time: ");
	 public static JTextField jt3 = new JTextField(10);
	 JLabel jl4=new JLabel(" Max Service Time: ");
	 public static JTextField jt4 = new JTextField(10);
	 JLabel jl5=new JLabel(" Min Serivce Time: ");
	 public static JTextField jt5 = new JTextField(10);
	 JLabel jl6=new JLabel(" NrClients: ");
	 public static JTextField jt6 = new JTextField(10);
	 
	 public JButton start = new JButton(" START ");
	 public JButton stop = new JButton(" STOP ");
	 
	 
		public QueueView(){
			//JPanel calcPanel = new JPanel();
		
		    this.setTitle("Queue Simulation");
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setSize(500, 270);
			
			mainBox = Box.createVerticalBox();

			hBox1 = Box.createHorizontalBox();
			hBox2 = Box.createHorizontalBox();
			hBox3 = Box.createHorizontalBox();
			hBox4 = Box.createHorizontalBox();
			hBox5 = Box.createHorizontalBox();
			hBox6 = Box.createHorizontalBox();
			hBox7 = Box.createHorizontalBox();
			hBox8 = Box.createHorizontalBox();
			
		
		
			hBox1.add(jl1);
			hBox1.add(jt1);
			
			hBox2.add(jl2);
			hBox2.add(jt2);
			
			hBox2.add(jl3);
			hBox2.add(jt3);
			
			hBox3.add(jl4);
			hBox3.add(jt4);
			
			hBox3.add(jl5);
			hBox3.add(jt5);
			
			hBox6.add(jl6);
			hBox6.add(jt6);
			
			hBox7.add(Box.createHorizontalStrut(5));
			hBox7.add(start);
			hBox7.add(Box.createHorizontalStrut(5));
			hBox7.add(stop);
			
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox1);
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox2);
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox3);
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox4);
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox5);
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox6);
			mainBox.add(Box.createVerticalStrut(5));
			mainBox.add(hBox7);
			mainBox.add(Box.createVerticalStrut(5));
			
		
			
			
			
			add(mainBox);
			
		}
		

	
		 
	public int getjt1(){
				
	    return Integer.parseInt(jt1.getText());
				
			}
	public int getjt2(){
				
		return Integer.parseInt(jt2.getText());
				
			}
	public int getjt3(){
		
		return Integer.parseInt(jt3.getText());
		
	}
	public int getjt4(){
		
		return Integer.parseInt(jt4.getText());
		
	}
	public int getjt5(){
		
		return Integer.parseInt(jt5.getText());
		
	}
	public int getjt6(){
		
		return Integer.parseInt(jt6.getText());
		
	}
		
		public void addListener(ActionListener a){
			
			start.addActionListener(a);
			stop.addActionListener(a);
			
			
		}
		
		
		
		public JButton getButtonStart() {
			return start;
		}
		
		public JButton getButtonStop() {
			return stop;
		}
		
		public JTextField getJT1() {
			return jt1;
		}
		public JTextField getJT2() {
			return jt2;
		}
		public JTextField getJT3() {
			return jt3;
		}
		public JTextField getJT4() {
			return jt4;
		}
		public JTextField getJT5() {
			return jt5;
		}
		public JTextField getJT6() {
			return jt6;
		}

}
